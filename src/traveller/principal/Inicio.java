package traveller.principal;

import traveller.dominio.SistemaImp;
import traveller.interfaz.ventana.VentanaDeveloper;
import traveller.interfaz.ventana.VentanaInicio;
import traveller.interfaz.ventana.VentanaLogin;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.UIManager;

/** 
 * La clase Inicio es la clase principal del sistema. 
 * Se encarga de manejar el ingreso e impresión de datos.
 *
 */
public class Inicio {

    private static SistemaImp sistema;

    public static void main(String[] args) {
	/* Vista estilo Windows. */
	try {
	    UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
	} catch (Exception e) {
	}

	/* Ventana de introducción al programa. */
	VentanaInicio inicio = new VentanaInicio();
	inicio.setLocationRelativeTo(null); // Centrar ventana.
	inicio.setVisible(true);

	/* Esperar dos segundos. */
	try {
	    Thread.sleep(2000);
	} catch (InterruptedException ex) {
	}

	inicio.setVisible(false); // Sacar la ventana de introducción.

	levantarSistema();
	if (sistema.isMostrarCartelInicio()) {
	    VentanaDeveloper ventana = new VentanaDeveloper(sistema);
	    ventana.setLocationRelativeTo(null); // Centrar ventana.
	    ventana.setVisible(true);
	} else {
	    VentanaLogin ventana = new VentanaLogin(sistema);
	    ventana.setLocationRelativeTo(null); // Centrar ventana.
	    ventana.setVisible(true);
	}
    }

    public static void levantarSistema() {
	try {
	    ObjectInputStream lecturaSistema = null;

	    lecturaSistema = new ObjectInputStream(new BufferedInputStream(new FileInputStream(
		    "sistema.txt")));
	    sistema = (SistemaImp) lecturaSistema.readObject();

	    lecturaSistema.close();
	} catch (Exception e) {
	    sistema = new SistemaImp();
	}
        
            Scanner s, c;
        try {
            // leer tipos de eventos de archivo externo
            s = new Scanner(new File("eventos.txt"));
             ArrayList<Object> listaTiposEventos = new ArrayList<Object>();
            while (s.hasNextLine()) {
                listaTiposEventos.add(s.nextLine());
            }
            sistema.setListaTiposEventos(listaTiposEventos);
            s.close();
            
            // leer ciudades de archivo externo
            c = new Scanner(new File("ciudades.txt"));
            ArrayList<Object> listaCiudades = new ArrayList<Object>();
            while (c.hasNextLine()) {
                listaCiudades.add(c.nextLine());
            }
            sistema.setListaCiudades(listaCiudades);
            c.close();
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Inicio.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
